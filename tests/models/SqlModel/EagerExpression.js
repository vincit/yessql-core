var expect = require('expect.js');
var EagerExpression = require('../../../models/SqlModel/EagerExpression');
var SqlModelQueryBuilder = require('../../../models/SqlModel/SqlModelQueryBuilder');
var EagerType = SqlModelQueryBuilder.EagerType;

describe('EagerExpression', function () {

  describe('parse', function () {

    function testParse(str, parsed) {
      expect(new EagerExpression(str).obj).to.eql(parsed);
    }

    it('empty string', function () {
      testParse('', {});
    });

    it('null', function () {
      testParse(null, {});
    });

    it('single relation', function () {
      testParse('a', {a: EagerType.None});
    });

    it('nested relations', function () {
      testParse('a.b', {a: {b: EagerType.None}});
      testParse('a.b.c', {a: {b: {c: EagerType.None}}});
    });

    it('multiple relations', function () {
      testParse('[a, b, c]', {a: EagerType.None, b: EagerType.None, c: EagerType.None});
    });

    it('multiple nested relations', function () {
      testParse('[a.b, c.d.e, f]', {
        a: {b: EagerType.None},
        c: {d: {e: EagerType.None}},
        f: EagerType.None
      });
    });

    it('multiple sub relations', function () {
      testParse('[a.[b, c.[d, e.f]], g]', {
        a: {
          b: EagerType.None,
          c: {
            d: EagerType.None,
            e: {
              f: EagerType.None
            }
          }
        },
        g: EagerType.None
      });
    });

    it('* into EagerType.AllRecursive', function () {
      testParse('*', EagerType.AllRecursive);
      testParse('a.b.*', {a: {b: EagerType.AllRecursive}});
      testParse('a.b.[c, d.*, e]', {
        a: {
          b: {
            c: EagerType.None,
            d: EagerType.AllRecursive,
            e: EagerType.None
          }
        }
      });
    });

    it('^ into EagerType.Recursive', function () {
      testParse('a.b.^', {a: {b: EagerType.Recursive}});
      testParse('a.b.[c, d.^, e]', {
        a: {
          b: {
            c: EagerType.None,
            d: EagerType.Recursive,
            e: EagerType.None
          }
        }
      });
    });

  });

  describe('#isSubTree', function () {

    function testSubTree(str, subStr) {
      it('"' + subStr + '" is a sub tree of "' + str + '"', function () {
        expect(new EagerExpression(str).isSubTree(subStr)).to.equal(true);
      });
    }

    function testNotSubTree(str, subStr) {
      it('"' + subStr + '" is not a sub tree of "' + str + '"', function () {
        expect(new EagerExpression(str).isSubTree(subStr)).to.equal(false);
      });
    }

    // Everything is a sub tree of *.
    testSubTree('*', 'a');
    testSubTree('*', '[a, b]');
    testSubTree('*', 'a.b');
    testSubTree('*', 'a.b.[c, d]');
    testSubTree('*', '[a, b.c, c.d.[e, f.g.[h, i]]]');
    testSubTree('*', '*');
    testSubTree('*', 'a.*');
    testSubTree('*', 'a.^');
    testSubTree('a.*', 'a');
    testSubTree('a.*', 'a.b');
    testSubTree('a.*', 'a.*');
    testSubTree('a.*', 'a.^');
    testSubTree('a.*', 'a.b.c');
    testSubTree('a.*', 'a.[b, c]');
    testSubTree('a.*', 'a.[b, c.d]');
    testSubTree('a.[b.*, c]', 'a.b.c.d');
    testSubTree('a.[b.*, c]', 'a.[b.c.d, c]');
    testSubTree('a.[b.*, c]', 'a.[b.[c, d], c]');
    testNotSubTree('a.*', 'b');
    testNotSubTree('a.*', 'c');
    testNotSubTree('a.*', '[a, b]');
    testNotSubTree('a.*', '*');
    testNotSubTree('a.[b.*, c]', 'a.[b.c.d, c.d]');

    // * in subtree requires * in tree.
    testSubTree('a.b.*', 'a.b.*');
    testNotSubTree('a.b.*', '*');
    testNotSubTree('a.b.*', 'a.*');
    testNotSubTree('a.b.*', 'a.[b.*, c]');

    // Equal.
    testSubTree('a', 'a');
    testSubTree('a.b', 'a.b');
    testSubTree('a.b.[c, d]', 'a.b.[c, d]');
    testSubTree('[a.b.[c, d], e]', '[a.b.[c, d], e]');

    // Subs.
    testSubTree('a.b', 'a');
    testNotSubTree('a', 'a.b');

    testSubTree('a.b.c', 'a');
    testSubTree('a.b.c', 'a.b');
    testNotSubTree('a', 'a.b.c');
    testNotSubTree('a.b', 'a.b.c');

    testSubTree('a.[b, c]', 'a');
    testSubTree('a.[b, c]', 'a.b');
    testSubTree('a.[b, c]', 'a.c');
    testNotSubTree('a.[b, c]', 'a.c.d');
    testNotSubTree('a.[b, c]', 'b');
    testNotSubTree('a.[b, c]', 'c');

    testSubTree('[a.b.[c, d.e], b]', 'a');
    testSubTree('[a.b.[c, d.e], b]', 'b');
    testSubTree('[a.b.[c, d.e], b]', 'a.b');
    testSubTree('[a.b.[c, d.e], b]', 'a.b.c');
    testSubTree('[a.b.[c, d.e], b]', 'a.b.d');
    testSubTree('[a.b.[c, d.e], b]', 'a.b.d.e');
    testSubTree('[a.b.[c, d.e], b]', '[a.b.[c, d], b]');
    testSubTree('[a.b.[c, d.e], b]', '[a.b.[c, d.[e]], b]');
    testNotSubTree('[a.b.[c, d.e], b]', 'c');
    testNotSubTree('[a.b.[c, d.e], b]', 'b.c');
    testNotSubTree('[a.b.[c, d.e], b]', '[a, b, c]');
    testNotSubTree('[a.b.[c, d.e], b]', 'a.b.e');
    testNotSubTree('[a.b.[c, d.e], b]', '[a.b.e, b]');
    testNotSubTree('[a.b.[c, d.e], b]', '[a.b.c, c]');
    testNotSubTree('[a.b.[c, d.e], b]', 'a.b.[c, e]');
    testNotSubTree('[a.b.[c, d.e], b]', 'a.b.[c, d, e]');
    testNotSubTree('[a.b.[c, d.e], b]', 'a.b.[c, d.[e, f]]');

    // EagerType.Recursive.
    testSubTree('a.^', 'a.^');
    testSubTree('a.^', 'a.a');
    testSubTree('a.^', 'a.a.^');
    testSubTree('a.^', 'a.a.a');
    testSubTree('a.^', 'a.a.a.^');
    testSubTree('[a.^, b.[c.^, d]]', 'a');
    testSubTree('[a.^, b.[c.^, d]]', 'b.c');
    testSubTree('[a.^, b.[c.^, d]]', 'b.c.^');
    testSubTree('[a.^, b.[c.^, d]]', '[a, b]');
    testSubTree('[a.^, b.[c.^, d]]', '[b.c, b.d]');
    testSubTree('[a.^, b.[c.^, d]]', '[b.c.c.c, b.d]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.^, b]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.a, b]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.a.^, b.c]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.a.^, b.c.^]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.a.^, b.c.c]');
    testSubTree('[a.^, b.[c.^, d]]', '[a.a.^, b.[c.c.c, d]]');
    testNotSubTree('a.^', 'b');
    testNotSubTree('a.^', 'a.b');
    testNotSubTree('a.^', 'a.a.b');
    testNotSubTree('a.^', 'a.a.b.^');
    testNotSubTree('[a.^, b.[c.^, d]]', 'a.b');
    testNotSubTree('[a.^, b.[c.^, d]]', '[c, b]');
    testNotSubTree('[a.^, b.[c.^, d]]', '[c, b]');
    testNotSubTree('[a.^, b.[c.^, d]]', 'b.c.d');

  });

});
