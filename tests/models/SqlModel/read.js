var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird');

module.exports = function (shared) {

  var Model1 = shared.modelClasses.Model1;
  var Model2 = shared.modelClasses.Model2;
  var models = shared.models;


  describe('#find()', function () {

    it('should find all when called without arguments', function () {
      return Model2.find().then(function (foundModels) {
        expect(foundModels).to.have.length(models.model2.length);
        for (var i = 0; i < foundModels.length; ++i) {
          expect(foundModels[i].$toDatabaseJson(true)).to.eql(models.model2[i].$toDatabaseJson(true));
        }
      });
    });

    it('can be chained with a where() method', function () {
      return Model2.find()
        .where('textProperty2', models.model2[2].textProperty2)
        .then(function (foundModels) {
          expect(foundModels).to.have.length(1);
          expectShallowEquality(models.model2[2], foundModels[0]);
        })
    });

    it('can be chained with a orWhere() method', function () {
      return Model2.find()
        .where('textProperty2', models.model2[2].textProperty2)
        .orWhere('textProperty2', models.model2[4].textProperty2)
        .then(function (foundModels) {
          expect(foundModels).to.have.length(2);
          expectShallowEquality(models.model2[2], foundModels[0]);
          expectShallowEquality(models.model2[4], foundModels[1]);
        });
    });

    it('can be chained with a whereIn() method', function () {
      return Model2.find()
        .whereIn('textProperty2', [models.model2[2].textProperty2, models.model2[3].textProperty2])
        .then(function (foundModels) {
          expect(foundModels).to.have.length(2);
          expectShallowEquality(models.model2[2], foundModels[0]);
          expectShallowEquality(models.model2[3], foundModels[1]);
        });
    });

    it('can be chained with a whereBetween() method', function () {
      return Model2.find()
        .whereBetween('textProperty2', [models.model2[2].textProperty2, models.model2[5].textProperty2])
        .then(function (foundModels) {
          expect(foundModels).to.have.length(4);
          expectShallowEquality(models.model2[2], foundModels[0]);
          expectShallowEquality(models.model2[3], foundModels[1]);
          expectShallowEquality(models.model2[4], foundModels[2]);
          expectShallowEquality(models.model2[5], foundModels[3]);
        });
    });

    it('can be chained with a select() method', function () {
      return Model2.find()
        .where('textProperty2', models.model2[2].textProperty2)
        .select('id')
        .then(function (foundModels) {
          expect(foundModels).to.have.length(1);
          expect(_.keys(foundModels[0])).to.eql(['id']);
          expect(foundModels[0].id).to.eql(models.model2[2].id);
        });
    });

    it('can be chained with a page() method (1)', function () {
      return Model2.find()
        .page(0, 2)
        .then(function (foundModels) {
          expect(foundModels.total).to.equal(6);
          expect(foundModels.results).to.have.length(2);
          expectShallowEquality(models.model2[0], foundModels.results[0]);
          expectShallowEquality(models.model2[1], foundModels.results[1]);
        });
    });

    it('can be chained with a page() method (2)', function () {
      return Model2.find()
        .page(1, 3)
        .then(function (foundModels) {
          expect(foundModels.total).to.equal(6);
          expect(foundModels.results).to.have.length(3);
          expectShallowEquality(models.model2[3], foundModels.results[0]);
          expectShallowEquality(models.model2[4], foundModels.results[1]);
          expectShallowEquality(models.model2[5], foundModels.results[2]);
        });
    });

    it('can be chained with a page() method (3)', function () {
      return Model2.find()
        .page(1, 4)
        .then(function (foundModels) {
          expect(foundModels.total).to.equal(6);
          expect(foundModels.results).to.have.length(2);
          expectShallowEquality(models.model2[4], foundModels.results[0]);
          expectShallowEquality(models.model2[5], foundModels.results[1]);
        });
    });

    it('should fetch all relations if query.eager(SqlModel.EagerType.AllRecursive) is called', function () {
      return Model1
        .find().where('id', models.model1[0].id)
        .eager(Model1.EagerType.AllRecursive)
        .then(function (foundModels) {
          expect(foundModels).to.have.length(1);

          expectShallowEquality(models.model2[0], foundModels[0].child);
          expectShallowEquality(models.model2[1], foundModels[0].customChild);
          expectShallowEquality(models.model2[2], foundModels[0].children1[0]);
          expectShallowEquality(models.model2[3], foundModels[0].children1[1]);
          expectShallowEquality(models.model2[2], foundModels[0].children1WithQuery[0]);
          expectShallowEquality(models.model2[4], foundModels[0].children2[0]);

          expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
          expectShallowEquality(models.model3[1], foundModels[0].children2[0].grandChildren2[0]);

          expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
          expectShallowEquality(models.model3[3], foundModels[0].children2[1].grandChildren2[0]);
          expectShallowEquality(models.model3[4], foundModels[0].children2[1].grandChildren2[1]);
          expectShallowEquality(models.model3[5], foundModels[0].children2[1].grandChildren2[2]);
          expectShallowEquality(models.model3[4], foundModels[0].children2[1].grandChildren2WithQuery[0]);
          expectShallowEquality(models.model3[5], foundModels[0].children2[1].grandChildren2WithQuery[1]);
        });
    });

    it('query.eager methods should accept an single relation name', function () {
      return Model1.find().where('id', models.model1[0].id).eager('child').then(function (foundModels) {
        expect(foundModels).to.have.length(1);
        expectShallowEquality(models.model2[0], foundModels[0].child);
        expect(foundModels[0]).to.not.have.property('customChild');
        expect(foundModels[0]).to.not.have.property('children1');
        expect(foundModels[0]).to.not.have.property('children2');
        expect(foundModels[0]).to.not.have.property('children1WithQuery');
      })
    });

    it('query.eager methods should accept an array of relation names', function () {
      return Model1.find().where('id', models.model1[0].id).eager(['child', 'children2']).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectToNotExist(foundModels[0].children2[0].grandChildren1);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren1);
        expectToNotExist(foundModels[0].children2[1].grandChildren2);
      });
    });

    it('query.eager methods should accept a list of relation names as arguments', function () {
      return Model1.find().where('id', models.model1[0].id).eager('child', 'children2').then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectToNotExist(foundModels[0].children2[0].grandChildren1);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren1);
        expectToNotExist(foundModels[0].children2[1].grandChildren2);
      });
    });

    it('query.eager methods should accept an object', function () {
      return Model1.find().where('id', models.model1[0].id).eager({
        child: [],
        children2: Model1.EagerType.None
      }).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectToNotExist(foundModels[0].children2[0].grandChildren1);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren1);
        expectToNotExist(foundModels[0].children2[1].grandChildren2);
      });
    });

    it('query.eager methods should accept an object and fetch relations recursively 1', function () {
      return Model1.find().where('id', models.model1[0].id).eager({
        child: [],
        children2: Model1.EagerType.AllRecursive
      }).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
        expectShallowEquality(models.model3[1], foundModels[0].children2[0].grandChildren2[0]);

        expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
        expectShallowEquality(models.model3[3], foundModels[0].children2[1].grandChildren2[0]);
        expectShallowEquality(models.model3[4], foundModels[0].children2[1].grandChildren2[1]);
        expectShallowEquality(models.model3[5], foundModels[0].children2[1].grandChildren2[2]);
      });
    });

    it('query.eager methods should accept an object and fetch relations recursively 2', function () {
      return Model1.find().where('id', models.model1[0].id).eager({
        child: [],
        children2: 'grandChildren1'
      }).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren2);
        expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
      });
    });

    it('query.eager methods should accept an object and fetch relations recursively 3', function () {
      return Model1.find().where('id', models.model1[0].id).eager({
        child: [],
        children2: ['grandChildren1']
      }).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren2);
        expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
      });
    });

    it('query.eager methods should accept an object and fetch relations recursively 4', function () {
      return Model1.find().where('id', models.model1[0].id).eager({
        child: [],
        children2: {
          grandChildren1: Model1.EagerType.AllRecursive
        }
      }).then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren2);
        expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
      });
    });

    it('query.eager methods should accept a string in EagerExpression format', function () {
      return Model1.find().where('id', models.model1[0].id).eager('[child, children2.[grandChildren1.*]]').then(function (foundModels) {
        expect(foundModels).to.have.length(1);

        expectShallowEquality(models.model2[0], foundModels[0].child);
        expectToNotExist(foundModels[0].customChild);
        expectToNotExist(foundModels[0].children1);

        expectShallowEquality(models.model2[4], foundModels[0].children2[0]);
        expectShallowEquality(models.model2[5], foundModels[0].children2[1]);

        expectShallowEquality(models.model3[0], foundModels[0].children2[0].grandChildren1[0]);
        expectToNotExist(foundModels[0].children2[0].grandChildren2);

        expectToNotExist(foundModels[0].children2[1].grandChildren2);
        expectShallowEquality(models.model3[2], foundModels[0].children2[1].grandChildren1[0]);
      });
    });

  });


  describe('#$findRelated()', function () {

    it('should fetch model1[0].child (HasOneRelation)', function () {
      return models.model1[0].$findRelated('child').then(function (model) {
        expectShallowEquality(model, models.model2[0]);
      });
    });

    it('should fail to fetch model1[0].child using a where clause (HasOneRelation)', function () {
      return models.model1[0].$findRelated('child').where('textProperty2', 'text4').then(function (model) {
        expect(model == null).to.eql(true);
      });
    });

    it('should fetch model1[0].children1 (HasManyRelation)', function () {
      return models.model1[0].$findRelated('children1').then(function (foundModels) {
        expect(foundModels).to.have.length(2);
        expectShallowEquality(foundModels[0], models.model2[2]);
        expectShallowEquality(foundModels[1], models.model2[3]);
      });
    });

    it('should fetch model1[0].children1[0] using a where clause (HasManyRelation)', function () {
      return models.model1[0].$findRelated('children1').where('textProperty2', models.model2[2].textProperty2).then(function (foundModels) {
        expect(foundModels).to.have.length(1);
        expectShallowEquality(foundModels[0], models.model2[2]);
      });
    });

    it('should fetch model1[0].children1WithQuery (HasManyRelation with custom query)', function () {
      return models.model1[0].$findRelated('children1WithQuery').then(function (foundModels) {
        expect(foundModels).to.have.length(1);
        expectShallowEquality(foundModels[0], models.model2[2]);
      });
    });

    it('should fetch model1[0].children2 (ManyToManyRelation)', function () {
      return models.model1[0].$findRelated('children2').then(function (foundModels) {
        expect(foundModels).to.have.length(2);
        expectShallowEquality(foundModels[0], models.model2[4]);
        expectShallowEquality(foundModels[1], models.model2[5]);
      });
    });

    it('should fetch model1[0].children2[1] using a where clause (ManyToManyRelation)', function () {
      return models.model1[0].$findRelated('children2').where('textProperty2', models.model2[5].textProperty2).then(function (foundModels) {
        expect(foundModels).to.have.length(1);
        expectShallowEquality(foundModels[0], models.model2[5]);
      });
    });

    it('should fetch model2[5].grandChildren2WithQuery (ManyToManyRelation with custom query)', function () {
      return models.model2[5].$findRelated('grandChildren2WithQuery').then(function (foundModels) {
        expect(foundModels).to.have.length(2);
        expectShallowEquality(foundModels[0], models.model3[4]);
        expectShallowEquality(foundModels[1], models.model3[5]);
      });
    });

    it('should find nothing (HasManyRelation)', function () {
      return models.model1[0].$findRelated('children1').where('textProperty2', 'INVALID TEXT').then(function (foundModels) {
        expect(foundModels).to.have.length(0);
      });
    });

    it('should find nothing (ManyToManyRelation)', function () {
      return models.model1[0].$findRelated('children2').where('textProperty2', 'INVALID TEXT').then(function (foundModels) {
        expect(foundModels).to.have.length(0);
      });
    });

  });


  describe('#$findOneRelated()', function () {

    it('should fetch model2[0] (HasOneRelation)', function (done) {
      models.model1[0].$findOneRelated('child').then(function (model) {
        expectShallowEquality(model, models.model2[0]);
        done();
      }).done();
    });

    it('should fetch model2[2] (HasManyRelation)', function (done) {
      models.model1[0].$findOneRelated('children1', function (query) {
        query.where('id', models.model2[2].id);
      }).then(function (model) {
        expectShallowEquality(model, models.model2[2]);
        done();
      }).done();
    });

    it('should fetch model2[4] (ManyToManyRelation)', function (done) {
      models.model1[0].$findOneRelated('children2').where('id', models.model2[5].id).then(function (model) {
        expectShallowEquality(model, models.model2[5]);
        done();
      }).done();
    });

  });
};

function expectShallowEquality(model1, model2) {
  expect(model1.$toJson(true)).to.eql(model2.$toJson(true));
}

function expectToNotExist(relation) {
  expect(relation).to.eql(undefined);
}
